<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class importCommand extends Command{

    protected static $defaultName = 'app:import';

    protected function configure(): void{
        
    }

    public function __construct(EntityManagerInterface $em)
    {
        $this->manager = $em;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        

        
        $io->success('La commande a été effectuer avec succès');
        return Command::SUCCESS;
    }
}