<?php

namespace App\Command;

use App\Entity\Meeting;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpcomingCommand extends Command{

    protected static $defaultName = 'app:upcomming';
    private $manager;
    private $mailer;

    protected function configure(): void{
        
    }

    public function __construct(EntityManagerInterface $em, MailerService $mailer)
    {
        $this->manager = $em;
        $this->mailer = $mailer;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        
        $meetingRepo = $this->manager->getRepository(Meeting::class);

        $elements = $meetingRepo->getUpcomming();

        if(count($elements) > 0){
            $uno = $elements[0];
            $user = $uno->getUser();

            $uno->setIsNotified(true);

            $this->manager->persist($uno);

            $this->manager->flush();

            $this->mailer->sendUpcommingMailer($uno, $user);
        }

        $io->success('La commande a été effectuer avec succès');
        return Command::SUCCESS;
    }
}