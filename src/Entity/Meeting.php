<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MeetingRepository;
use App\Controller\MonthStatController;
use App\Controller\GlobalstatsController;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * @ORM\Entity(repositoryClass=MeetingRepository::class)
 * @ApiResource(
 *  normalizationContext={"groups"={"meeting:get"}},
 *  itemOperations={
 *      "get",  
 *      "put"={
 *          "denormalization_context"={"groups"={"meeting:put"}}
 *      },
 *      "delete"
 *  },
 *  collectionOperations={
 *      "get",
 *      "post"={    
 *          "denormalization_context"={"groups"={"meeting:post"}}
 *      },
 *      "globalStats"={
 *          "path"="/meetings/stats",
 *          "method"="get",
 *          "controller"=GlobalstatsController::class,
 *          "openapi_context"={
 *              "summary"="Get global stats",
 *              "example"="true"
 *          }
 *      },
 *      "MonthlyStats"={
 *          "path"="/meetings/monthly",
 *          "method"="get",
 *          "controller"=MonthStatController::class,
 *          "openapi_context"={
 *              "summary"="Get Monhtly stats",
 *              "example"="true"
 *          }
 *      }
 *  }
 * )
 * @ApiFilter(
 *  BooleanFilter::class,
 *  properties={
 *      "retro",
 *      "isAborted"
 *  }
 * )
 * @ApiFilter(
 *  DateFilter::class,
 *  properties={
 *      "startedAt",
 *      "endsAt"
 *  }
 * )
 * @ApiFilter(
 *  RangeFilter::class,
 *  properties={
 *      "startedAt"
 *  }
 * )
 * @ApiFilter(
 *  OrderFilter::class,
 *  properties={
 *      "startedAt",
 *      "createdAt"
 *  }
 * )
 */
class Meeting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"meeting:get"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"meeting:post", "meeting:put", "meeting:get"})
     */
    private $startedAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"meeting:post", "meeting:put", "meeting:get"})
     */
    private $endsAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"meeting:post", "meeting:put", "meeting:get"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"meeting:post", "meeting:put", "meeting:get"})
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"meeting:post", "meeting:put", "meeting:get"})
     */
    private $retro;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"meeting:post", "meeting:put", "meeting:get"})
     */
    private $badge;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"meeting:get"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"meeting:get", "meeting:put"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"meeting:put", "meeting:get"})
     */
    private $isAborted = 0;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"meeting:get"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @Groups({"meeting:get", "meeting:put"})
     */
    private $updatedUser;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isNotified;

    /**
     * @Groups({"meeting:get"})
     */
    private $isEditable;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        //$this->isAborted = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getEndsAt(): ?\DateTimeInterface
    {
        return $this->endsAt;
    }

    public function setEndsAt(\DateTimeInterface $endsAt): self
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getRetro(): ?bool
    {
        return $this->retro;
    }

    public function setRetro(?bool $retro): self
    {
        $this->retro = $retro;

        return $this;
    }

    public function getBadge(): ?string
    {
        return $this->badge;
    }

    public function setBadge(?string $badge): self
    {
        $this->badge = $badge;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsAborted(): ?bool
    {
        return $this->isAborted;
    }

    public function setIsAborted(?bool $isAborted): self
    {
        $this->isAborted = $isAborted;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUpdatedUser(): ?User
    {
        return $this->updatedUser;
    }

    public function setUpdatedUser(?User $updatedUser): self
    {
        $this->updatedUser = $updatedUser;

        return $this;
    }

    public function getIsNotified(): ?bool
    {
        return $this->isNotified;
    }

    public function setIsNotified(?bool $isNotified): self
    {
        $this->isNotified = $isNotified;

        return $this;
    }

    public function getIsEditable(): ?bool
    {
        return $this->isEditable;
    }

    public function setIsEditable(?bool $isEditable): self
    {
        $this->isEditable = $isEditable;

        return $this;
    }
}
