<?php

namespace App\DataPersister;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Meeting;
use Symfony\Component\Security\Core\Security;

class MeetingDataPersister implements ContextAwareDataPersisterInterface
{
    private $_entityManager;
    private $_request;
    private $_security;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $r,
        Security $security
    )
    {
        $this->_entityManager = $entityManager;
        $this->_request = $r;
        $this->_security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Meeting;
    }

    /**
     * @param Article $data
     */
    public function persist($data, array $context = [])
    {
        //$r = $this->_request->getContent();

        $user = $this->_security->getUser();

        $meetingRepo = $this->_entityManager->getRepository(Meeting::class);

        if($data->getId()){
            if($context['item_operation_name'] === 'put'){
                $data->setUpdatedAt(new \DateTime());
                $data->setUpdatedUser($user);
            }
        }else{
            $data->setUser($user);
        }

        $startedDateElement = $meetingRepo->getEventOnDate($data->getStartedAt()->format('Y-m-d H:i'));
        $endsDateElement = $meetingRepo->getEventOnDate($data->getEndsAt()->format('Y-m-d H:i'));

        if(count($startedDateElement) > 1 || count($endsDateElement) > 1){
            throw new \Exception("La date choisie correspond à une réunion", 30);
        }else{
            $startedDateElement = $startedDateElement[0];
            $endsDateElement = $endsDateElement[0];
            if($startedDateElement->getId() != $data->getId() || $endsDateElement->getId() != $data->getId()){
                throw new \Exception("La date choisie correspond à une réunion", 30);
            }
        }
        
        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}