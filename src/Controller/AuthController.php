<?php

namespace App\Controller;

use App\Entity\RefreshToken;
use App\Entity\User;
use Firebase\JWT\JWT;
use App\Repository\UserRepository;
use App\Service\LdapService;
use App\Service\RefreshTokenService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AuthController extends AbstractController
{

    /**
     * @Route("/auth/refresh", name="token_refresh")
     */
    public function refresh(Request $r, RefreshTokenService $refreshTokenService){

        $manager = $this->getDoctrine()->getManager();
        $refreshRepo = $manager->getRepository(RefreshToken::class);

        $data = json_decode($r->getContent());
        $refresh_token = $data->refresh_token;
        $token = $data->token;

        $refresh_token = $refreshTokenService->decrypt($refresh_token, $this->getParameter('jwt_secret'));

        $refresh = $refreshRepo->findOneBy(['token'=>$token, 'refresh'=>$refresh_token]);

        if(!$refresh){
            return $this->json([
                'success' => false,
                'message' => 'Invalid token refresh'
            ], 401);
        }

        $me = $refresh->getUser();
        $payload = [
            "user" => $me->getUsername(),
            "exp"  => (new \DateTime())->modify("+1 hour")->getTimestamp(),
        ];
    
        $jwt = JWT::encode($payload, $this->getParameter('jwt_secret'), 'HS256');

        $new_refresh = $refresh = $refreshTokenService->createRefresh($me, sprintf('Bearer %s', $jwt), $this->getParameter('jwt_secret'));
        
        return $this->json([
            'success' => true,
            'message' => 'success',
            'token' => sprintf('Bearer %s', $jwt),
            'token_refresh' => $new_refresh
        ]);
    }

    /**
     * @Route("/auth/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $password = $request->get('password');
        $email = $request->get('username');
        $user = new User();
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setUsername($email);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->json([
            'user' => $user->getUsername()
        ]);
    }

    /**
     * @Route("/auth/login", name="login", methods={"POST"})
     */
    public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {

        $manager = $this->getDoctrine()->getManager();

        $userData = json_decode($request->getContent());
        $username = $userData->username;
        $password = $userData->password;
        $ldapService = new LdapService();
        $result = $ldapService->connect($username, $password);
        if($result){

            $user = $userRepository->findOneBy([
                'username' => $username,
            ]);
            if(!$user){
                $user = new User();
                $user->setUsername($username);
                $user->setPassword($encoder->encodePassword($user, $password));
                $user->setRoles([$result['role']]);
                $user->setSamaccountname($result['samaccountname'][0]);
                $user->setUserprincipalename($result['userprincipalname'][0]);
                $user->setDn($result['dn']);
                if(isset($result['mail'][0])){
                    $user->setEmail($result['mail'][0]);
                }

                $manager->persist($user);
                $manager->flush();
            }

            $refreshTokenService = new RefreshTokenService($this->getDoctrine()->getManager());
            
            if (!$user || !$encoder->isPasswordValid($user, $password)) {
                return $this->json([
                    'message' => 'Nom d\'utilisateur ou mot de passe incorrect',
                ]);
            }
            $payload = [
               "user" => $user->getUsername(),
               "exp"  => (new \DateTime())->modify("+1 hour")->getTimestamp(),
            ];
            $jwt = JWT::encode($payload, $this->getParameter('jwt_secret'), 'HS256');
            $refresh = $refreshTokenService->createRefresh($user, sprintf('Bearer %s', $jwt), $this->getParameter('jwt_secret'));
            return $this->json([
                'success' => true,
                'message' => 'success',
                'token' => sprintf('Bearer %s', $jwt),
                'token_refresh' => $refresh,
                'user' => [
                    'username' => $user->getUsername(),
                    'samaccountname' => $user->getSamaccountname(),
                    'principalename' => $user->getUserprincipalename(),
                    'dn' => $user->getDn()
                ]
            ]);
        }else{
            return $this->json([
                'success' => false,
                'message' => 'Nom d\'utilisateur ou mot de passe incorrect'
            ], 401);
        }
    }
}
