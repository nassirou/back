<?php

namespace App\Controller;

use App\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GlobalstatsController extends AbstractController
{
    public function __invoke()
    {
        $meetingsRepo = $this->getDoctrine()->getManager()->getRepository(Meeting::class);

        $now = new \DateTime();

        return [
            [
                'total' => $meetingsRepo->count([]),
                'coming' => $meetingsRepo->getTotalUpcomming()
            ]
        ];
    }
}
