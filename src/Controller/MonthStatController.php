<?php

namespace App\Controller;

use App\Entity\Meeting;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MonthStatController extends AbstractController
{
    public function __invoke()
    {
        $meetingRepo = $this->getDoctrine()->getManager()->getRepository(Meeting::class);
        return [
            $meetingRepo->getMonthlyStats()
        ];
    }
}
