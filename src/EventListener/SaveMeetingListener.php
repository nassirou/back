<?php

namespace App\EventListener;

use App\Entity\Meeting;
use App\Service\MailerService;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

class SaveMeetingListener
{
    private $mailer;
    private $user;

    public function __construct(MailerService $mailer, Security $security)
    {
        $this->mailer = $mailer;
        $this->user = $security->getUser();
    }

    public function __invoke(Meeting $meeting, LifecycleEventArgs $event): void {
        if($meeting->getRetro()){
            $this->mailer->sendCreationMailer($meeting, $this->user);
        }
    }
}