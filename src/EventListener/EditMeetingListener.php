<?php
namespace App\EventListener;

use App\Entity\Meeting;
use App\Service\MailerService;
use Symfony\Component\Security\Core\Security;

use Doctrine\ORM\Event\PreUpdateEventArgs;

class EditMeetingListener
{
    private $mailer;
    private $user;

    public function __construct(MailerService $mailer, Security $security)
    {
        $this->mailer = $mailer;
        $this->user = $security->getUser();
    }

    public function preUpdate(Meeting $meeting, PreUpdateEventArgs $eventArgs){

        $creator = $meeting->getUser();

        if($this->user->getId() !== $creator->getId()){
            $editeds = $eventArgs->getEntityChangeSet();

            if(isset($editeds['isAborted']) && ($editeds['isAborted'][1] === true)){
                $this->mailer->sendDeleteMail($meeting, $this->user);
            }
    
            if(isset($editeds['startedAt'])){
                $this->mailer->notifyEdition($meeting, $this->user);
            }
        }
    }
}