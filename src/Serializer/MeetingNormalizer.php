<?php

namespace App\Serializer;

use App\Entity\Meeting;
use App\Service\RoleService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class MeetingNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{

    use NormalizerAwareTrait;

    private const ALREADY_CALLED_MEETING_NORMALIZER = 'AppMeetingNormalizerAlreadyCalled';
    private $security;
    private $manager;
    private $roleService;

    public function __construct(EntityManagerInterface $manager, Security $secure, RoleService $roleService)
    {
        $this->manager = $manager;
        $this->security = $secure;
        $this->roleService = $roleService;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return !isset($context[self::ALREADY_CALLED_MEETING_NORMALIZER]) && $data instanceof Meeting;
    }

    /**
     * @param Meeting $object
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED_MEETING_NORMALIZER] = true;

        $me = $this->security->getUser();
        $creator = $object->getUser();

        if($me && $creator){
            $object->setIsEditable($this->roleService->under($me, $creator));
        }else{
            $object->setIsEditable(false);
        }

        return $this->normalizer->normalize($object, $format, $context);
    }
}