<?php

namespace App\Repository;

use App\Entity\Meeting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Meeting|null find($id, $lockMode = null, $lockVersion = null)
 * @method Meeting|null findOneBy(array $criteria, array $orderBy = null)
 * @method Meeting[]    findAll()
 * @method Meeting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeetingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meeting::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Meeting $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getEventOnDate($date){
        return $this->createQueryBuilder('m')
            ->where('m.startedAt <= :date')
            ->setParameter('date', $date)
            ->andWhere('m.endsAt >= :date2')
            ->setParameter('date2', $date)
            ->getQuery()
            ->getResult();
    }

    public function getTotalUpcomming(){
        $now = new \DateTime('now');

        return $this->createQueryBuilder('m')
            ->select('COUNT(m.id) as total')
            ->andWhere('m.startedAt > :val')
            ->setParameter('val', $now->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getOneOrNullResult()['total'];
    }

    public function getMonthlyStats(){
        $now = new \DateTime('now');

        $monthStars = new \DateTime('now');
        $monthStars->modify('first day of this month');

        $monthEnds = new \DateTime('now');
        $monthEnds->modify('last day of this month');

        $all = $this->createQueryBuilder('m')
            ->select('COUNT(m.id) as total')
            ->andWhere('m.startedAt > :val')
            ->andWhere('m.startedAt < :val2')
            ->setParameter('val', $monthStars->format('Y-m-d H:i:s'))
            ->setParameter('val2', $monthEnds->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getOneOrNullResult()['total'];

        $oncoming = $this->createQueryBuilder('m')
            ->select('COUNT(m.id) as total')
            ->andWhere('m.startedAt > :val')
            ->andWhere('m.startedAt < :val2')
            ->setParameter('val', $now->format('Y-m-d H:i:s'))
            ->setParameter('val2', $monthEnds->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getOneOrNullResult()['total'];

        $passed = $this->createQueryBuilder('m')
            ->select('COUNT(m.id) as total')
            ->andWhere('m.startedAt > :val')
            ->andWhere('m.startedAt < :val2')
            ->setParameter('val', $monthStars->format('Y-m-d H:i:s'))
            ->setParameter('val2', $now->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getOneOrNullResult()['total'];

        return [
            'all' => $all,
            'coming' => $oncoming,
            'done' => $passed
        ];
    }

    public function getUpcomming(){

        $now = new \DateTime('now');

        $nextHour = new \DateTime();
        date_modify($nextHour, '+1 hour');

        return $this->createQueryBuilder('m')
            ->andWhere('m.startedAt > :val')
            ->andWhere('m.startedAt < :val2')
            ->setParameter('val', $now->format('Y-m-d H:i:s'))
            ->setParameter('val2', $nextHour->format('Y-m-d H:i:s'))
            ->andWhere('m.isNotified is null')
            ->getQuery()
            ->getResult();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Meeting $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Meeting[] Returns an array of Meeting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Meeting
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
