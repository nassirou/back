<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Meeting;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailerService{

    private $mailer;
    private $sender;
    private $officialReceiver;

    public function __construct(MailerInterface $mailer){
        $this->sender = $_SERVER['SENDER_EMAIL'];
        $this->officialReceiver = $_SERVER['RECEIVER_EMAIL'];
        $this->mailer = $mailer;
    }

    public function sendCreationMailer(Meeting $meeting, User $user){
        $email = (new TemplatedEmail())
            ->from($this->sender)
            ->to($this->officialReceiver)
            ->subject('Nouvelle réservation (' . $meeting->getTitle() . ')')
            ->htmlTemplate('index.html.twig')
            ->context([
                "company" => $_SERVER['APP_NAME'],
                "title" => $meeting->getTitle(),
                "start" => $meeting->getStartedAt()->format('d/m/Y H:i'),
                "end" => $meeting->getEndsAt()->format('d/m/Y H:i'),
                "retro" => $meeting->getRetro(),
                "comment" => $meeting->getComment(),
                "created" => $meeting->getCreatedAt()->format('d/m/Y H:i'),
                "creator" => ucfirst($user->getUsername())
            ]);
        
        $this->mailer->send($email);
    }

    public function sendUpcommingMailer(Meeting $meeting, User $user){
        $email = (new TemplatedEmail())
        ->from($this->sender)
        ->to($this->officialReceiver)
        ->subject('Réunion imminente (' . $meeting->getTitle() . ')')
        ->htmlTemplate('upcomming.html.twig')
        ->context([
            "company" => $_SERVER['APP_NAME'],
            "title" => $meeting->getTitle(),
            "start" => $meeting->getStartedAt()->format('d/m/Y H:i'),
            "end" => $meeting->getEndsAt()->format('d/m/Y H:i'),
            "retro" => $meeting->getRetro(),
            "comment" => $meeting->getComment(),
            "created" => $meeting->getCreatedAt()->format('d/m/Y H:i'),
            "creator" => ucfirst($user->getUsername())
        ]);
    
        $this->mailer->send($email);
    }
    
    public function sendDeleteMail(Meeting $meeting, User $editor){
        if($meeting->getUser()->getEmail()){
            $email = (new TemplatedEmail())
            ->from($this->sender)
            ->to($meeting->getUser()->getEmail())
            ->subject('Réservation annulée (' . $meeting->getTitle() . ')')
            ->htmlTemplate('delete.html.twig')
            ->context([
                "company" => $_SERVER['APP_NAME'],
                "title" => $meeting->getTitle(),
                "start" => $meeting->getStartedAt()->format('d/m/Y H:i'),
                "end" => $meeting->getEndsAt()->format('d/m/Y H:i'),
                "retro" => $meeting->getRetro(),
                "comment" => $meeting->getComment(),
                "created" => $meeting->getCreatedAt()->format('d/m/Y H:i'),
                "creator" => ucfirst($meeting->getUser()->getUsername()),
                "editor" => ucfirst($editor->getUsername())
            ]);
        
            $this->mailer->send($email);
        }
    }

    public function notifyEdition(Meeting $meeting, User $editor){
        if($meeting->getUser()->getEmail()){
            $email = (new TemplatedEmail())
            ->from($this->sender)
            ->to($meeting->getUser()->getEmail())
            ->subject('Réservation reportée (' . $meeting->getTitle() . ')')
            ->htmlTemplate('notify.html.twig')
            ->context([
                "company" => $_SERVER['APP_NAME'],
                "title" => $meeting->getTitle(),
                "start" => $meeting->getStartedAt()->format('d/m/Y H:i'),
                "end" => $meeting->getEndsAt()->format('d/m/Y H:i'),
                "retro" => $meeting->getRetro(),
                "comment" => $meeting->getComment(),
                "created" => $meeting->getCreatedAt()->format('d/m/Y H:i'),
                "creator" => ucfirst($meeting->getUser()->getUsername()),
                "editor" => ucfirst($editor->getUsername())
            ]);
        
            $this->mailer->send($email);
        }
    }
}