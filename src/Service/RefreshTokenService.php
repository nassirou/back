<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\RefreshToken;
use Doctrine\ORM\EntityManagerInterface;

class RefreshTokenService{

    private $manager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->manager = $em;
    }

    public function createRefresh(User $user, $token, $secret): string {
        
        $sha_code = uniqid();

        $refresh = $this->manager->getRepository(RefreshToken::class)->findOneBy(['user' => $user]);

        if($refresh){
            $now = new \DateTime();

            if($refresh->getExpiredAt() > $now){
                $refresh->setToken($token)
                        ->setExpiredAt((new \DateTime())->modify("+12 hour"));

                $sha_code = $refresh->getRefresh();
            }else{
                $refresh->setToken($token)
                    ->setRefresh($sha_code)
                    ->setExpiredAt((new \DateTime())->modify("+12 hour"));
            }
        }else{
            $refresh = new RefreshToken();

            $refresh->setUser($user)
                ->setToken($token)
                ->setRefresh($sha_code)
                ->setExpiredAt((new \DateTime())->modify("+12 hour"));
        }
        
        $this->manager->persist($refresh);
        $this->manager->flush();

        return $this->encrypt($sha_code, $secret);
    }


    public function encrypt($simple_string, $encryption_key){
        $ciphering = "AES-128-CTR";
        
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;
        
        $encryption_iv = '1234567891011121';
        
        $encryption = openssl_encrypt($simple_string, $ciphering,
                    $encryption_key, $options, $encryption_iv);
        
        return $encryption;
    }

    public function decrypt($encryption, $decryption_key){
        $ciphering = "AES-128-CTR";
        $decryption_iv = '1234567891011121';
        
        $options = 0;
        
        $decryption = openssl_decrypt ($encryption, $ciphering, 
        $decryption_key, $options, $decryption_iv);

        return $decryption;
    }
}