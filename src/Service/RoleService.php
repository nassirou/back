<?php

namespace App\Service;

use App\Entity\User;

class RoleService{

    private $roles = [
        'Superviseur',
        'RH',
        'Informatique',
        'Charge de projet',
        'Evenmedia',
        'Direction'
    ];


    public function under(User $current, User $second): bool {
        $firstRole = $current->getRoles()[0];
        $secondRole = $second->getRoles()[0];

        if($current->getId() === $second->getId()){
            return true;
        }else{
            foreach($this->roles as $key => $value){
                if($value === $firstRole){
                    $firstRole = $key;
                }
                if($value === $secondRole){
                    $secondRole = $key;
                }
            }
    
            return $firstRole > $secondRole;
        }
    }
}