<?php

namespace App\Service;

class LdapService {
    private $host;
    private $port;
    private $ldap;
    private $dn;

    public function __construct()
    {
        $this->host = $_SERVER['LDAP_HOST'];
        $this->port = $_SERVER['LDAP_PORT'];
        {
            $serveur = $_SERVER['LDAP_SERVER'];
            $break = explode('.', $serveur);
            $this->dn = "dc=" . $break[0] . ",dc=" . $break[1];
        }
        $this->ldap = ldap_connect($this->host, $this->port);
    }

    public function connect(String $username, String $password){
        $groups = [
            'Superviseur',
            'RH',
            'Informatique',
            'Charge de projet',
            'Evenmedia',
            'Direction'
        ];

        try{
            if(ldap_bind($this->ldap, $username, $password)){
                $finded = null;
                $role = null;
                foreach($groups as $value){
                    $dn = "OU=$value," . $this->dn;
                    $filter="(|(sn=$username)(cn=$username))";
                    $restriction = array( "cn", "sn", "mail", "userprincipalname", "useraccountcontrol", "samaccountname", "mail");
                    $result = ldap_search($this->ldap, $dn, $filter, $restriction);
                    $entries = ldap_get_entries($this->ldap, $result);
                
                    if(isset($entries[0])){
                        $finded = $entries[0];
                        $role = $value;
                        break;
                    }
                }
            
                if($finded){
                    $finded['role'] = $role;
                    return $finded;
                }
                return false;
            }
        }catch(\Throwable $t){
            return false;
        }
        return false;
    }
}